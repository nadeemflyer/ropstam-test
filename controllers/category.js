const { validationResult } = require('express-validator');
const {resp_generator} =  require('../helpers/utils');
const categoryModel = require('../models/categories');
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const categoryList = async  (req, response) => {
    try {
        const v_errors = validationResult(req)
        if (!v_errors.isEmpty()) {
            console.log('Got validation  Error', v_errors)
            return resp_generator(response, '', 'error', 'Please try with correct data', 400)
        }

        // Column List for ordering purpose
        const columnList = ['id','name']
        const draw = req.query.draw;
        const start = parseInt(req.query.start);
        const limit = parseInt(req.query.length);
        const column_name = columnList[req.query.order[0]['column']];
        const  column_sort_order = req.query.order[0]['dir'];        
        const search_value = req.query.search['value'];
    
        const  whereStatement = {status : 1};
        // If Search
        if(search_value !== null){
            whereStatement.name = {[Op.like]: '%' + search_value + '%'};
        }
        const cars = await categoryModel.findAll({
            where: whereStatement,
            offset: start, 
            limit: limit,
            order: [[column_name, column_sort_order]],
        });

        // Count Number for Rows for Pagination purpose
        const totalRecord = await categoryModel.count({
            where: {
                status : 1
            }
        });
        const output = {
            'draw' : draw,
            'recordsTotal' : totalRecord,
            'recordsFiltered' : cars.length,
            'data' : cars
        };
        
        return resp_generator(response, output , 'success', '')

    } catch (e) {
        console.log('Got Error', e)
        return resp_generator(response, '', 'error', 'Network error Please try again Later', 500)

    }


};

const addCategory = async  (req, response) => {
    try {
        const v_errors = validationResult(req)
        if (!v_errors.isEmpty()) {
            console.log('Got validation  Error', v_errors)
            return resp_generator(response, '', 'error', 'Please try with correct data', 400)
        }

        await categoryModel.create({ name: req.body.name});

        return resp_generator(response, '', 'success', 'Category Added  Successfully')

    } catch (e) {
        console.log('Got Error', e)
        return resp_generator(response, '', 'error', 'Network error Please try again Later', 500)

    }


};

const updateCategory = async  (req, response) => {
    try {
        const v_errors = validationResult(req)
        if (!v_errors.isEmpty()) {
            console.log('Got validation  Error', v_errors)
            return resp_generator(response, '', 'error', 'Please try with correct data', 400)
        }

     
        category = await categoryModel.findOne({
            where: {
                id: req.body.id,
                status : 1
            }
        });
        if(category === null){
            return resp_generator(response, '', 'error', 'No record found')
        } 
        await categoryModel.update({ name: req.body.name }, {
            where: {
              id: req.body.id
            }
          });

       
        
        return resp_generator(response, '', 'success', 'Category Updated  Successfully')

    } catch (e) {
        console.log('Got Error', e)
        return resp_generator(response, '', 'error', 'Network error Please try again Later', 500)

    }


};

const deleteCategory = async  (req, response) => {
    try {
        const v_errors = validationResult(req)
        if (!v_errors.isEmpty()) {
            console.log('Got validation  Error', v_errors)
            return resp_generator(response, '', 'error', 'Please try with correct data', 400)
        }

        category = await categoryModel.findOne({
            where: {
                id: req.body.id,
                status : 1
            }
        });
        if(category === null){
            return resp_generator(response, '', 'error', 'No record found')
        } 

        // Soft Delete Car Data
        await categoryModel.update({ status: 0 }, {
            where: {
              id: req.body.id
            }
        });
        
        return resp_generator(response, '', 'success', 'Category Deleted  Successfully')

    } catch (e) {
        console.log('Got Error', e)
        return resp_generator(response, '', 'error', 'Network error Please try again Later', 500)

    }


};

module.exports = {
    categoryList,
    addCategory,
    updateCategory,
    deleteCategory
}