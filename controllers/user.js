const { validationResult } = require('express-validator');
const {SentEmail, resp_generator} =  require('../helpers/utils');
const userModel = require('../models/users')
require('dotenv').config();
//const moment = require('moment');
const jwt= require('jsonwebtoken');
const jwtSecret = process.env.JWT_SECRET_KEY 
const bcrypt = require('bcryptjs')

const genAccessToken = (user) => {
    const userId = user.id;
    const email = user.email;
    const tokenPayload = { userId, email };
    console.log(tokenPayload, '-----------',jwtSecret )
    // Generate Token on the base of User id and email for 10 Minutes 
    const accessToken = jwt.sign(tokenPayload, jwtSecret, {expiresIn: 600});
    return accessToken;
}

register = async  (req, response) => {
    try {
        const v_errors = validationResult(req)
        if (!v_errors.isEmpty()) {
            console.log('Got validation  Error', v_errors)
            return resp_generator(response, '', 'error', 'Please try with correct data', 400)
        }
        isDuplicate = await userModel.findOne({
            where: {
                email: req.body.email
            }
        });
       
        if (isDuplicate !== null) {
            return resp_generator(response, 'error', 'Record Already exist')
        }

        // Generate a random Password
        var randromPassword = Math.random().toString(36).slice(-8);
        const hash_password = await bcrypt.hash(randromPassword, 8)
         
        await userModel.create({ name: req.body.full_name, email: req.body.email, password : hash_password });
        // Send Email to user
        const emailBody = `Hi ${req.body.full_name},\r\n Your Ropstam testing app password is ${randromPassword}`
        SentEmail(req.body.email,emailBody)
         
        return resp_generator(response, '', 'success', 'Registration Successfully Plase check your email for password.')

    } catch (e) {
        console.log('Got Error', e)
        return resp_generator(response, '', 'error', 'Network error Please try again Later', 500)

    }


};



const signin = async  (req, response) => {
    try {
        const v_errors = validationResult(req)
        if (!v_errors.isEmpty()) {
            console.log('Got validation  Error', v_errors)
            return resp_generator(response, '', 'error', 'Please try with correct data', 400)
        }

        // Check For duplicate Entry
        user = await userModel.findOne({
            where: {
                email: req.body.email
            }
        });

        if (user === null) {
            return resp_generator(response, 'error', 'Invalid username  or Password')
        }

        if (!bcrypt.compareSync(req.body.password, user.password)) {
            return resp_generator(response, 'error','', 'Invalid username or Password')
        }
        
        // Create Token
        const accessToken = genAccessToken(user);
        return resp_generator(response, accessToken, 'success', '')

    } catch (e) {
        console.log('Got Error', e)
        return resp_generator(response, '', 'error', 'Network error Please try again Later', 500)

    }


};
module.exports = {
    register,
    signin
}