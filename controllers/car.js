const { validationResult } = require('express-validator');
const {resp_generator} =  require('../helpers/utils');
const {carModel, categoriesModel} = require('../models');
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;

const carList = async  (req, response) => {
    try {
        const v_errors = validationResult(req)
        if (!v_errors.isEmpty()) {
            console.log('Got validation  Error', v_errors)
            return resp_generator(response, '', 'error', 'Please try with correct data', 400)
        }
        // Column List for ordering purpose
        const columnList = ['id','name', 'category', 'make', 'color', 'registration']
        const draw = req.query.draw;
        const start = parseInt(req.query.start);
        const limit = parseInt(req.query.length);
        const column_name = columnList[req.query.order[0]['column']];
        const  column_sort_order = req.query.order[0]['dir'];        
        const search_value = req.query.search['value'];
    
        const  whereStatement = {status : 1};
        console.log('search is ', typeof search_value)
        // If Search
        if(search_value !== ''){
            whereStatement.name = {[Op.like]: '%' + search_value + '%'};
            whereStatement.make = {[Op.like]: '%' + search_value + '%'};
            whereStatement.registration = {[Op.like]: '%' + search_value + '%'};
        }

        // Join Car and  Category model with DataTables Functionality
        const cars = await carModel.findAll({
            include: [{
                model: categoriesModel,
                as: "cat_id",
                required: true
              }],
            where: whereStatement,
            offset: start, 
            limit: limit,
            order: [[column_name, column_sort_order]],
        });
        
        // Count Number for Rows for Pagination purpose
        const totalRecord =  await carModel.count({
            where: {
                status : 1
            }
          });

        const output = {
            'draw' : draw,
            'recordsTotal' : totalRecord,
            'recordsFiltered' : cars.length,
            'data' : cars
        };

        return resp_generator(response, output, 'success', '')

    } catch (e) {
        console.log('Got Error', e)
        return resp_generator(response, '', 'error', 'Network error Please try again Later', 500)

    }


};

const addCar = async  (req, response) => {
    try {
        const v_errors = validationResult(req)
        if (!v_errors.isEmpty()) {
            console.log('Got validation  Error', v_errors)
            return resp_generator(response, '', 'error', 'Please try with correct data', 400)
        }

        await carModel.create({ 
            name: req.body.name, category : req.body.category, make : req.body.make, color : req.body.color, registration: req.body.registration 
        });
        
        return resp_generator(response, '', 'success', 'Car Added  Successfully')

    } catch (e) {
        console.log('Got Error', e)
        return resp_generator(response, '', 'error', 'Network error Please try again Later', 500)

    }


};

const updateCar = async  (req, response) => {
    try {
        const v_errors = validationResult(req)
        if (!v_errors.isEmpty()) {
            console.log('Got validation  Error', v_errors)
            return resp_generator(response, '', 'error', 'Please try with correct data', 400)
        }

        car = await carModel.findOne({
            where: {
                id: req.body.id,
                status : 1
            }
        });
        if(car === null){
            return resp_generator(response, '', 'error', 'No record found')
        } 
        await carModel.update({ 
            name: req.body.name, category : req.body.category, make : req.body.make, color : req.body.color, registration: req.body.registration 
        },
        {
            where: {
                id: req.body.id
            }
        });

        return resp_generator(response, '', 'success', 'Car Updated  Successfully')

    } catch (e) {
        console.log('Got Error', e)
        return resp_generator(response, '', 'error', 'Network error Please try again Later', 500)

    }


};

const deleteCar = async  (req, response) => {
    try {
        const v_errors = validationResult(req)
        if (!v_errors.isEmpty()) {
            console.log('Got validation  Error', v_errors)
            return resp_generator(response, '', 'error', 'Please try with correct data', 400)
        }

        car = await carModel.findOne({
            where: {
                id: req.body.id,
                status : 1
            }
        });
        if(car === null){
            return resp_generator(response, '', 'error', 'No record found')
        } 

        // Soft Delete Car Data
        await carModel.update({ status: 0 }, {
            where: {
              id: req.body.id
            }
        });
        
        return resp_generator(response, '', 'success', 'Car Deleted  Successfully')

    } catch (e) {
        console.log('Got Error', e)
        return resp_generator(response, '', 'error', 'Network error Please try again Later', 500)

    }


};

module.exports = {
    carList,
    addCar,
    updateCar,
    deleteCar
}