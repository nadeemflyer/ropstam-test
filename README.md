# Test App for Ropstam
## Key Features
* Car CRUD
* Category CRUD
* User Sign & Sign up
* Authorization & Authentication
## Stack
* Node.js with Express.js Framework
* Mysql
* Sequelize ORM

## How To run
* Change env  file configurations
* npm install
* npm run dev
