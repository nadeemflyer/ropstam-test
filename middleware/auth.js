const jwt = require("jsonwebtoken");
const {resp_generator} =  require('../helpers/utils');
// dotenv is required to read the  environment variables
require('dotenv').config();
const config = process.env;

function checkToken(req, response, next) {
  const token = req.headers.authorization;
  // Error Response if Token is not submitted
  if (!token) {
    return resp_generator(response, 'error', 'Invalid Login', 401)
  }
  // Verify Token
  try {
    const tokenPayload = jwt.verify(token, config.JWT_SECRET_KEY);
    response.user = tokenPayload;
    next();
  }catch (error) {
    return resp_generator(response, 'error', 'Invalid Login', 401)
  }
}
module.exports = checkToken