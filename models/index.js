const userModel  = require('./users');
const categoriesModel  = require('./categories');
const carModel  = require('./cars');

// Associate Car Model with Categories
carModel.belongsTo(categoriesModel, {
  as : 'cat_id',
  foreignKey: 'id'
})
// categoriesModel.hasMany(carModel, { foreignKey: 'cat_id', targetKey: 'cat_id' });

const models = {
    userModel,
    categoriesModel,
    carModel

  };
module.exports = models
