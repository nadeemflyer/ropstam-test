//const Sequelize = require('sequelize')
const {sequelize,Sequelize } = require('../db/config');
// Schema Definitation For cars Table
const categories = sequelize.define('categories', {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    status: {
        type: Sequelize.BOOLEAN,
        defaultValue : 1,
        allowNull: false
    }
  });
  
  module.exports = categories;