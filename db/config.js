require('dotenv').config();
const Sequelize = require("sequelize");
const config = process.env;
const sequelize = new Sequelize(config.DB_NAME, config.DB_USERNAME, config.DB_PASSWORD, {
  host: config.HOST,
  dialect: config.DIALTEC,
  operatorsAliases: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 3000,
    idle: 1000
  }
});
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
module.exports = {Sequelize, sequelize}