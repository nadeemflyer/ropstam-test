const express = require("express");
const router = express.Router();
const carController = require('../controllers/car');
const checkAuth = require("../middleware/auth");

const { body,query } = require('express-validator');
console.log('hello')

// checkAuth is used to Authenticate user
router.get('/',
    checkAuth,
    query('start').notEmpty().isInt(),
    query('length').notEmpty().isInt(),
    query('search[value]').optional().escape(),
    query('order[0][column]').notEmpty().isInt(),
    query('order[0][dir]').notEmpty().isIn(['ASC','DESC']),
    query('draw').notEmpty().isInt(),
    carController.carList
)
.post('/',
    checkAuth,
    body('name').notEmpty().escape(),
    carController.addCar
)
.put('/',
    checkAuth,
    body('id').notEmpty().isInt(), 
    body('name').notEmpty().escape(), 
    body('category').notEmpty().escape(), 
    body('make').notEmpty().escape(), 
    body('color').notEmpty().escape(), 
    body('registration').notEmpty().escape(),
    carController.updateCar
)
.delete('/',
checkAuth,
body('id').notEmpty().isInt(), 
carController.deleteCar
)

module.exports =  router