const express = require("express");
const router = express.Router();
const categoryController = require('../controllers/category');
const checkAuth = require("../middleware/auth");

const { body, query } = require('express-validator');

router.get('/',
    checkAuth,
    query('start').notEmpty().isInt(),
    query('length').notEmpty().isInt(),
    query('search[value]').optional().escape(),
    query('order[0][column]').notEmpty().isInt(),
    query('order[0][dir]').notEmpty().isIn(['ASC','DESC']),
    query('draw').notEmpty().isInt(),
    categoryController.categoryList
)
.post('/',
    checkAuth,
    body('name').notEmpty().escape(),
    categoryController.addCategory
)
.put('/',
    checkAuth,
    body('id').notEmpty().isInt(), 
    body('name').notEmpty().escape(),
    categoryController.updateCategory
)
.delete('/',
    checkAuth,
    body('id').notEmpty().isInt(), 
    categoryController.deleteCategory
)

module.exports =  router
