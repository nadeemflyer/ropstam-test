const express = require("express");
const router = express.Router();
const UserController = require('../controllers/user');

const { body } = require('express-validator');


 router.post("/register",
    body('full_name').notEmpty().escape(), 
    body('email').notEmpty().escape(),
    UserController.register
);

router.post("/signin",
    body('password').notEmpty().escape(),
    body('email').notEmpty().escape(),
    UserController.signin
);
  module.exports =  router




