const express = require('express')
const app = express()
const bodyParser = require('body-parser');
const cors = require('cors');

// Accept JSON data
app.use(express.json()); 
app.use(bodyParser.urlencoded());
//  Make it Public and Allow Cross Domain Calls
app.use(cors({
  origin: '*'
}));

const UserRoutes = require('./routes/userRoutes')
const carRoutes = require('./routes/carRoutes')
const categoryRoutes = require('./routes/categoryRoutes')
const {sequelize } = require('./db/config');
const models = require('./models');

app.use((err, req, response, next) => {

    // To  Catch JSON related Erros
    if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
        console.error(err);
        return resp_generator(response, '', 'error', 'Network error Please try again Later', 500)
    }
    next();
});

app.use("/user", UserRoutes);
app.use("/car", carRoutes);
app.use("/category", categoryRoutes);
let port = 3000

app.listen(port, () => {
    sequelize.sync({ force: true })
  .then(() => {
    models.categoriesModel.create({
        name: 'Hatchback ',
      });
      models.categoriesModel.create({
        name: 'Sedan ',
      });
      models.categoriesModel.create({
        name: 'SUV',
      });
  })
    console.log(`App listening at http://localhost:${port}`)
})
